/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uaspbo.koperasi;

/**
 *
 * @author user
 */
// Simpanan.java

import java.util.Date;

public class Simpanan {
    private Date tanggal;
    private double jumlah;
    private Anggota anggota;

    public Simpanan(Date tanggal, double jumlah, Anggota anggota) {
        this.tanggal = tanggal;
        this.jumlah = jumlah;
        this.anggota = anggota;
    }

    // getters and setters
    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public double getJumlah() {
        return jumlah;
    }

    public void setJumlah(double jumlah) {
        this.jumlah = jumlah;
    }

    public Anggota getAnggota() {
        return anggota;
    }

    public void setAnggota(Anggota anggota) {
        this.anggota = anggota;
    }
}
