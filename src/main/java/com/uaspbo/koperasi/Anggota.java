/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uaspbo.koperasi;

/**
 *
 * @author user
 */
// Anggota.java
public class Anggota {
    private String nama;
    private String alamat;
    private double saldoSimpanan;

    public Anggota(String nama, String alamat) {
        this.nama = nama;
        this.alamat = alamat;
        this.saldoSimpanan = 0.0;
    }

    // getters and setters
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public double getSaldoSimpanan() {
        return saldoSimpanan;
    }

    public void setSaldoSimpanan(double saldoSimpanan) {
        this.saldoSimpanan = saldoSimpanan;
    }

    public void tambahSimpanan(double jumlah) {
        saldoSimpanan += jumlah;
    }

    public void ajukanPinjaman(double jumlah) {
        // logika untuk mengajukan pinjaman
    }
}
