/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uaspbo.koperasi;

/**
 *
 * @author user
 */

import java.util.Date;
import java.util.Scanner;

public class Menu {
    private static Scanner scanner = new Scanner(System.in);
    private static Koperasi koperasi = Koperasi.getInstance();

        public void tampilkanMenu() {
            boolean isExit = false;
            while (!isExit) {
                System.out.println("===== MENU KOPERASI =====");
                System.out.println("1. Tambah Anggota");
                System.out.println("2. Tambah Simpanan");
                System.out.println("3. Ajukan Pinjaman");
                System.out.println("4. Proses Transaksi");
                System.out.println("5. Informasi Anggota");
                System.out.println("6. Informasi Simpanan");
                System.out.println("7. Informasi Pinjaman");
                System.out.println("8. Informasi Transaksi");
                System.out.println("9. Informasi Keseluruhan");
                System.out.println("10. Keluar");
                System.out.print("Pilih menu: ");
                int choice = scanner.nextInt();
                scanner.nextLine(); // Membersihkan newline
    
                switch (choice) {
                    case 1:
                        tambahAnggota();
                        break;
                    case 2:
                        tambahSimpanan();
                        break;
                    case 3:
                        ajukanPinjaman();
                        break;
                    case 4:
                        prosesTransaksi();
                        break;
                    case 5:
                        informasiAnggota();
                        break;
                    case 6:
                        informasiSimpanan();
                        break;
                    case 7:
                        informasiPinjaman();
                        break;
                    case 8:
                        informasiTransaksi();
                        break;
                    case 9:
                        tampilkanKeseluruhanInformasi();
                        break;
                    case 10:
                        isExit = true;
                        System.out.println("Terima kasih telah menggunakan program Koperasi.");
                        break;
                    default:
                        System.out.println("Pilihan tidak valid.");
                        break;
                }
            }    
    }

    public static void tambahAnggota() {
        System.out.print("Nama Anggota: ");
        String nama = scanner.nextLine();
        System.out.print("Alamat Anggota: ");
        String alamat = scanner.nextLine();

        Anggota anggota = new Anggota(nama, alamat);
        koperasi.tambahAnggota(anggota);
        System.out.println("Anggota berhasil ditambahkan.");
    }

    public static void tambahSimpanan() {
        System.out.print("Nama Anggota: ");
        String namaAnggota = scanner.nextLine();
        System.out.print("Jumlah Simpanan: ");
        double jumlah = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline

        Anggota anggota = cariAnggota(namaAnggota);
        if (anggota != null) {
            Simpanan simpanan = new Simpanan(new Date(), jumlah, anggota);
            anggota.tambahSimpanan(jumlah);
            koperasi.tambahSimpanan(simpanan);
            System.out.println("Simpanan berhasil ditambahkan.");
        } else {
            System.out.println("Anggota tidak ditemukan.");
        }
    }

    public static void ajukanPinjaman() {
        System.out.print("Nama Anggota: ");
        String namaAnggota = scanner.nextLine();
        System.out.print("Jumlah Pinjaman: ");
        double jumlah = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline

        Anggota anggota = cariAnggota(namaAnggota);
        if (anggota != null) {
            Pinjaman pinjaman = new Pinjaman(new Date(), jumlah, anggota);
            anggota.ajukanPinjaman(jumlah);
            koperasi.ajukanPinjaman(pinjaman);
            System.out.println("Pinjaman berhasil diajukan.");
        } else {
            System.out.println("Anggota tidak ditemukan.");
        }
    }

    public static void prosesTransaksi() {
        System.out.print("Nama Anggota: ");
        String namaAnggota = scanner.nextLine();
        System.out.print("Keterangan Transaksi: ");
        String keterangan = scanner.nextLine();

        Anggota anggota = cariAnggota(namaAnggota);
        if (anggota != null) {
            Transaksi transaksi = new Transaksi(new Date(), keterangan, anggota);
            koperasi.prosesTransaksi(transaksi);
            System.out.println("Transaksi berhasil diproses.");
        } else {
            System.out.println("Anggota tidak ditemukan.");
        }
    }

    public static Anggota cariAnggota(String namaAnggota) {
        for (Anggota anggota : koperasi.getDaftarAnggota()) {
            if (anggota.getNama().equalsIgnoreCase(namaAnggota)) {
                return anggota;
            }
        }
        return null;
    }

    public static void informasiAnggota() {
        System.out.println("===== INFORMASI ANGGOTA =====");
        for (Anggota anggota : koperasi.getDaftarAnggota()) {
            System.out.println("Nama: " + anggota.getNama());
            System.out.println("Alamat: " + anggota.getAlamat());
            System.out.println("Saldo Simpanan: " + anggota.getSaldoSimpanan());
            System.out.println("-----------------------------");
        }
    }

    public static void informasiSimpanan() {
        System.out.println("===== INFORMASI SIMPANAN =====");
        for (Simpanan simpanan : koperasi.getDaftarSimpanan()) {
            System.out.println("Tanggal: " + simpanan.getTanggal());
            System.out.println("Jumlah: " + simpanan.getJumlah());
            System.out.println("Anggota: " + simpanan.getAnggota().getNama());
            System.out.println("-----------------------------");
        }
    }

    public static void informasiPinjaman() {
        System.out.println("===== INFORMASI PINJAMAN =====");
        for (Pinjaman pinjaman : koperasi.getDaftarPinjaman()) {
            System.out.println("Tanggal: " + pinjaman.getTanggal());
            System.out.println("Jumlah: " + pinjaman.getJumlah());
            System.out.println("Anggota: " + pinjaman.getAnggota().getNama());
            System.out.println("-----------------------------");
        }
    }

    public static void informasiTransaksi() {
        System.out.println("===== INFORMASI TRANSAKSI =====");
        for (Transaksi transaksi : koperasi.getDaftarTransaksi()) {
            System.out.println("Tanggal: " + transaksi.getTanggal());
            System.out.println("Keterangan: " + transaksi.getKeterangan());
            System.out.println("Anggota: " + transaksi.getAnggota().getNama());
            System.out.println("-----------------------------");
        }
    }

    public static void tampilkanKeseluruhanInformasi() {
        System.out.println("===== KESELURUHAN INFORMASI =====");
        informasiAnggota();
        informasiSimpanan();
        informasiPinjaman();
        informasiTransaksi();
    }
}
