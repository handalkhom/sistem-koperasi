/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uaspbo.koperasi;

/**
 *
 * @author user
 */
// Transaksi.java

import java.util.Date;

public class Transaksi {
    private Date tanggal;
    private String keterangan;
    private Anggota anggota;

    public Transaksi(Date tanggal, String keterangan, Anggota anggota) {
        this.tanggal = tanggal;
        this.keterangan = keterangan;
        this.anggota = anggota;
    }

    // getters and setters
    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Anggota getAnggota() {
        return anggota;
    }

    public void setAnggota(Anggota anggota) {
        this.anggota = anggota;
    }
}

