/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.uaspbo.koperasi;

/**
 *
 * @author user
 */
// Koperasi.java

import java.util.ArrayList;
import java.util.List;

public class Koperasi {
    private static Koperasi instance;
    private String nama;
    private String alamat;
    private List<Anggota> daftarAnggota;
    private List<Simpanan> daftarSimpanan;
    private List<Pinjaman> daftarPinjaman;
    private List<Transaksi> daftarTransaksi;

    private Koperasi() {
        daftarAnggota = new ArrayList<>();
        daftarSimpanan = new ArrayList<>();
        daftarPinjaman = new ArrayList<>();
        daftarTransaksi = new ArrayList<>();
    }

    public static Koperasi getInstance() {
        if (instance == null) {
            instance = new Koperasi();
        }
        return instance;
    }

    // getters and setters
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public List<Anggota> getDaftarAnggota() {
        return daftarAnggota;
    }

    public void setDaftarAnggota(List<Anggota> daftarAnggota) {
        this.daftarAnggota = daftarAnggota;
    }

    public List<Simpanan> getDaftarSimpanan() {
        return daftarSimpanan;
    }

    public void setDaftarSimpanan(List<Simpanan> daftarSimpanan) {
        this.daftarSimpanan = daftarSimpanan;
    }

    public List<Pinjaman> getDaftarPinjaman() {
        return daftarPinjaman;
    }

    public void setDaftarPinjaman(List<Pinjaman> daftarPinjaman) {
        this.daftarPinjaman = daftarPinjaman;
    }

    public List<Transaksi> getDaftarTransaksi() {
        return daftarTransaksi;
    }

    public void setDaftarTransaksi(List<Transaksi> daftarTransaksi) {
        this.daftarTransaksi = daftarTransaksi;
    }

    public void tambahAnggota(Anggota anggota) {
        daftarAnggota.add(anggota);
    }

    public void tambahSimpanan(Simpanan simpanan) {
        daftarSimpanan.add(simpanan);
    }

    public void ajukanPinjaman(Pinjaman pinjaman) {
        daftarPinjaman.add(pinjaman);
    }

    public void prosesTransaksi(Transaksi transaksi) {
        daftarTransaksi.add(transaksi);
    }
}
