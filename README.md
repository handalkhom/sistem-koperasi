# Project UAS PBO

## Kelompok 4

Anggota:
- Handal Khomsyat          (1217050061)
- Muhammad Gifa Algifari   (1217050096)

## Project

Project yang dibuat adalah sebuah Sistem Koperasi yang memiliki batasan sebagai berikut
1. Menambahkan Anggota
2. Mencatat dan Melihat Transaksi, Pinjaman, dan Simpanan setiap Anggota

## Link Video Demo

https://youtu.be/JhH43f6JX08

## Design Pattern
Design Pattern yang digunakan pada projek ini adalah Singleton yang digunakan dalam kelas Koperasi dalam sistem koperasi. Penerapan Singleton memastikan bahwa hanya ada satu instance dari kelas Koperasi yang dapat diakses selama aplikasi berjalan.

## Class Diagram
```mermaid
classDiagram
    class Koperasi {
        -instance: Koperasi
        -nama: String
        -alamat: String
        -daftarAnggota: List<Anggota>
        -daftarSimpanan: List<Simpanan>
        -daftarPinjaman: List<Pinjaman>
        -daftarTransaksi: List<Transaksi>
        +getInstance(): Koperasi
        +getNama(): String
        +setNama(nama: String): void
        +getAlamat(): String
        +setAlamat(alamat: String): void
        +getDaftarAnggota(): List<Anggota>
        +setDaftarAnggota(daftarAnggota: List<Anggota>): void
        +getDaftarSimpanan(): List<Simpanan>
        +setDaftarSimpanan(daftarSimpanan: List<Simpanan>): void
        +getDaftarPinjaman(): List<Pinjaman>
        +setDaftarPinjaman(daftarPinjaman: List<Pinjaman>): void
        +getDaftarTransaksi(): List<Transaksi>
        +setDaftarTransaksi(daftarTransaksi: List<Transaksi>): void
        +tambahAnggota(anggota: Anggota): void
        +tambahSimpanan(simpanan: Simpanan): void
        +ajukanPinjaman(pinjaman: Pinjaman): void
        +prosesTransaksi(transaksi: Transaksi): void
    }

    class Anggota {
        -nama: String
        -alamat: String
        -saldoSimpanan: double
        +Anggota(nama: String, alamat: String)
        +getNama(): String
        +setNama(nama: String): void
        +getAlamat(): String
        +setAlamat(alamat: String): void
        +getSaldoSimpanan(): double
        +setSaldoSimpanan(saldoSimpanan: double): void
        +tambahSimpanan(jumlah: double): void
        +ajukanPinjaman(jumlah: double): void
    }

    class Simpanan {
        -tanggal: Date
        -jumlah: double
        -anggota: Anggota
        +Simpanan(tanggal: Date, jumlah: double, anggota: Anggota)
        +getTanggal(): Date
        +setTanggal(tanggal: Date): void
        +getJumlah(): double
        +setJumlah(jumlah: double): void
        +getAnggota(): Anggota
        +setAnggota(anggota: Anggota): void
    }

    class Pinjaman {
        -tanggal: Date
        -jumlah: double
        -anggota: Anggota
        +Pinjaman(tanggal: Date, jumlah: double, anggota: Anggota)
        +getTanggal(): Date
        +setTanggal(tanggal: Date): void
        +getJumlah(): double
        +setJumlah(jumlah: double): void
        +getAnggota(): Anggota
        +setAnggota(anggota: Anggota): void
    }

    class Transaksi {
        -tanggal: Date
        -keterangan: String
        -anggota: Anggota
        +Transaksi(tanggal: Date, keterangan: String, anggota: Anggota)
        +getTanggal(): Date
        +setTanggal(tanggal: Date): void
        +getKeterangan(): String
        +setKeterangan(keterangan: String): void
        +getAnggota(): Anggota
        +setAnggota(anggota: Anggota): void
    }

    Koperasi "1" --> "*" Anggota
    Koperasi "1" --> "*" Simpanan
    Koperasi "1" --> "*" Pinjaman
    Koperasi "1" --> "*" Transaksi

```
